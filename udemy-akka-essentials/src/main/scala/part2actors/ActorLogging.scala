package part2actors

import akka.actor.{Actor, ActorLogging, ActorSystem, Props}
import akka.event.Logging

object ActorLoggingDemo extends App {

  class SImpleActorWithExplicitLogger extends Actor {
    // explicit loggin
    val logger = Logging(context.system, this)

    override def receive: Receive = {
      /*
      1 - DEBUG
      2 - INFO
      3 - WARNING/WARN
      4 - ERROR
       */
      case message => logger.info(message.toString) // LOG it
    }
  }

  val system = ActorSystem("LoggingDemo")
  val actor = system.actorOf(Props[SImpleActorWithExplicitLogger])

  actor ! "Logging a simple message"

  // #2 - ActorLogging
  class ActorWithLogging extends Actor with ActorLogging {
    override def receive: Receive = {
      case (a, b) => log.info("Two thing: {} and {}", a, b) // Two things: 2 and 3
      case message => log.info(message.toString)
    }
  }

  val simplerActor = system.actorOf(Props[ActorWithLogging])
  simplerActor ! "Loggin a simple message by extending a trait"

  simplerActor ! (42, 45)

}
