package part1recap

object GeneralRecap extends App {

  val aConditon: Boolean = false
  var aVariable = 32
  aVariable += 1

  //Functional programming

  val incrementer = new Function1[Int, Int] {
    override def apply(v1: Int): Int = v1 + 1
  }

  val incremented = incrementer(42) //43
  //incrementer.apply(42)

  val anonymousIncrementer = (x: Int) => x + 1
  // Int => Int === Function1[Int, Int]

  //FP is all about working with functions as first-class
  List(1,2,3).map(incrementer)
  // map = HOF

  // for comprehensions
  val pairs = for {
    num <- List(1, 2, 3, 4)
    char <- List('a', 'b', 'c', 'd')
  } yield num + "-" + char

  // List(1,2,3,4).flatMap(num => List('a', 'b', 'c', 'd').map(char => num + "-" + char)

  // Seq, Array, List, Vector, Map, Tuples, Sets

  // "collections"
      // Option and TRy
  val anOption = Some(2)
  /*val aTry = Try {
    throw new RuntimeException
  }*/

  //pattern matching
  val unknow = 2
  val order = unknow match {
    case 1 => "first"
    case 2 => "second"
    case _ => "unknown"
  }

 // val bob = Person("Bob", 22)

}
