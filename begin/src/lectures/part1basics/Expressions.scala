package lectures.part1basics

object Expressions extends App {
  
  
  val x = 1 + 2//EXPRESSION
  println(x)
  println(2 + 3 * 4)
  //+ - * / & | ^ << >> >>> (right shift with zero extension)
  println(1 == x)
  // == != > < >= <=
  println(!(1==x))
  // ! }} &&
  
  var aVariable = 2
  aVariable +=3
  println(aVariable)
  
  //Instructions (DO) vs Expressions
  
  //IF expression
  val aCondition = true
  val aConditionValue = if (aCondition) 5 else 3
  println("aConditionValue: " + aConditionValue)
  println (if (aCondition) 5 else 3)
  println(1 + 3)
  
  var i = 0
  val aWhile = while ( i < 10){ //Unit Type
    println(i)
    i += 1
  }
  
  //everything in Scala is an expression!
  
  val aWeirdValue = (aVariable = 3) // Unit === void
  println(aWeirdValue)
  //side effects: println(), whiles, reassigning
  
  //Code blocks
  val aCodeBlock = {
    val y = 2
    val z = y + 1
    
    if (z > 2) "hello" else "goodbye"
  }
  //The value of the block is the value of its last expresion
  
  //Instrructiond are executed (think Java), expressions are evaluated (Scala)
  //Don't use while loops in your Scala code or It'll haunt you
  
  
  //1. Different between "hello world" and println("hello world")
  //2.
  val someValues = {
    2 < 3
  }
  println(someValues)
  
  val someOtherValues = {
    if (someValues) 239 else 986
    42 //The value of the block is the value of its last expresion
  }
  println(someOtherValues)
}