package lectures.part1basics

object ValuesVariablesTypes extends App {
  
  val x: Int = 42
  println(x)
  // VALS are immutable
  //COMPLIER can inter types (val x: Int = ... or val x = ...)
  
  val aString: String = "hello"
  val anotherString = "goodbye"
  
  val aBoolean: Boolean = false
  val aChar: Char = 'a'
  val aInt: Int = x
  val aShort: Short = 4613
  val aLong: Long = 5273985273895237L
  val aFloat: Float = 2.0f
  val aDouble: Double = 3.14
  
  //VARIABLES
  var aVariable: Int = 4
  aVariable = 6
  println(aVariable)
  //prefer vals over vars
}