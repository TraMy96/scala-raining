package lectures.part1basics

object CBNvsCBV extends App {
  
  def calledByValue(x: Long): Unit = {
    println("by value: " + x)
    println("by value :" + x)
  }
  
  def calledByName(x: => Long): Unit = {
    println("by name: " + x)
    println("by name :" + x)
  }
  
  calledByValue(System.nanoTime())
  calledByName(System.nanoTime())
  
  def infinite(): Int = 1 + infinite()
  def printFirst(x: Int, y: => Int) = println(x)
  
  // printFirst(infinite(), 34) => stack overflow error
  printFirst(34, infinite())
  /*
   * 1. Call By Value:
   * 		Value is computed before call
   * 		Same value use everywhere
   * 2. Call By Name
   * 		Expression is passed literally
   * 		Expression is evaluated at every use within
   */
}