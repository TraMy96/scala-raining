package lectures.part2oop

object Objects {
  
  //SCALA DOES NOT HAVE CLASS-LEVEL FUNCTIONALITY ("static")
  object Person { // type + its only instance
    // "static"/"class" - level functionality
    val N_EYES = 2
    def canFly: Boolean = false
    
    def apply(mother: Person, father: Person): Person = new Person("Bobbie")
  }
  
  class Person(val name: String) {
    // instance-level functionality
  }
  // COMPANIONS
  
  def main(args: Array[String]): Unit = {
    println(Person.N_EYES)
    println(Person.canFly)
    
    // Scala object = SINGLETON INSTANCE
    val mary = new Person("Mary")
    val john = new Person("John")
    println(mary == john)
    
    val person1 = Person
    val person2 = Person
    println(person1 == person2)
    
    val bobbie = Person.apply(mary, john)
  }
  //Scala Application = Scala object with
  // def main(args: Array[String]): Unit
  
  /*
   * Scala doesn't have "static" values/methods
   * Scala objects:
   * 		are in their own class
   * 		are the only instance
   * singleton pattern in one line
   * Scala companions:
   * 		can access each others private members
   * 		Scala is more OO than Java
   * Scala applications
   */
}