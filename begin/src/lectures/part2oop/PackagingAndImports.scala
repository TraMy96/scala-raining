package lectures.part2oop

import playground.Cinderella

object PackagingAndImports extends App {
  
  // package members are accessibe by their simple name
  val writer = new Writer("Daniel", "RockTheJVM", 2018)
  
  // import the package
  val princess = new Cinderella
  
}